#
#
#            Nim's Runtime Library
#        (c) Copyright 2015 Dominik Picheta
#
#    See the file "copying.txt", included in this
#    distribution, for details about the copyright.
#

## This module implements a high performance asynchronous HTTP server.
##
## This HTTP server has not been designed to be used in production, but
## for testing applications locally. Because of this, when deploying your
## application you should use a reverse proxy (for example nginx) instead of
## allowing users to connect directly to this server.
##
## Basic usage
## ===========
##
## This example will create an HTTP server on port 8080. The server will
## respond to all requests with a ``200 OK`` response code and "Hello World"
## as the response body.
##
## .. code-block::nim
##    import asynchttpserver, asyncdispatch
##
##    var server = newAsyncHttpServer()
##    proc cb(req: Request) {.async.} =
##      await req.respond(Http200, "Hello World")
##
##    waitFor server.serve(Port(8080), cb)

import tables, net, parseutils, uri, strutils
import httpcore

export httpcore except parseHeader

const
  maxLine = 8*1024

# TODO: If it turns out that the decisions that asynchttpserver makes
# explicitly, about whether to close the client sockets or upgrade them are
# wrong, then add a return value which determines what to do for the callback.
# Also, maybe move `client` out of `Request` object and into the args for
# the proc.
type
  Request* = ref object
    client*: Socket # TODO: Separate this into a Response object?
    reqMethod*: HttpMethod
    headers*: HttpHeaders
    protocol*: tuple[orig: string, major, minor: int]
    url*: Uri
    hostname*: string ## The hostname of the client that made the request.
    body*: string

  HttpServer* = ref object
    socket: Socket
    reuseAddr: bool
    reusePort: bool
    maxBody: int ## The maximum content-length that will be read for the body.

proc newHttpServer*(reuseAddr = true, reusePort = false,
                                maxBody = 8388608): HttpServer =
  ## Creates a new ``HttpServer`` instance.
  new result
  result.reuseAddr = reuseAddr
  result.reusePort = reusePort
  result.maxBody = maxBody

proc addHeaders(msg: var string, headers: HttpHeaders) =
  for k, v in headers:
    msg.add(k & ": " & v & "\c\L")

proc sendHeaders*(req: Request, headers: HttpHeaders): void =
  ## Sends the specified headers to the requesting client.
  var msg = ""
  addHeaders(msg, headers)
  req.client.send(msg)

proc respond*(req: Request, code: HttpCode, content: string,
              headers: HttpHeaders = nil): void =
  ## Responds to the request with the specified ``HttpCode``, headers and
  ## content.
  ##
  ## This procedure will **not** close the client socket.
  ##
  ## Example:
  ##
  ## .. code-block::nim
  ##    import json
  ##    proc handler(req: Request) {.async.} =
  ##      if req.url.path == "/hello-world":
  ##        let msg = %* {"message": "Hello World"}
  ##        let headers = newHttpHeaders([("Content-Type","application/json")])
  ##        await req.respond(Http200, $msg, headers)
  ##      else:
  ##        await req.respond(Http404, "Not Found")
  var msg = "HTTP/1.1 " & $code & "\c\L"

  if headers != nil:
    msg.addHeaders(headers)
  msg.add("Content-Length: ")
  # this particular way saves allocations:
  msg.add content.len
  msg.add "\c\L\c\L"
  msg.add(content)
  req.client.send(msg)

proc respondError(req: Request, code: HttpCode): void =
  ## Responds to the request with the specified ``HttpCode``.
  let content = $code
  var msg = "HTTP/1.1 " & content & "\c\L"

  msg.add("Content-Length: " & $content.len & "\c\L\c\L")
  msg.add(content)
  req.client.send(msg)

proc parseProtocol(protocol: string): tuple[orig: string, major, minor: int] =
  var i = protocol.skipIgnoreCase("HTTP/")
  if i != 5:
    raise newException(ValueError, "Invalid request protocol. Got: " &
        protocol)
  result.orig = protocol
  i.inc protocol.parseSaturatedNatural(result.major, i)
  i.inc # Skip .
  i.inc protocol.parseSaturatedNatural(result.minor, i)

proc sendStatus(client: Socket, status: string): void =
  client.send("HTTP/1.1 " & status & "\c\L\c\L")

proc parseUppercaseMethod(name: string): HttpMethod =
  result =
    case name
    of "GET": HttpGet
    of "POST": HttpPost
    of "HEAD": HttpHead
    of "PUT": HttpPut
    of "DELETE": HttpDelete
    of "PATCH": HttpPatch
    of "OPTIONS": HttpOptions
    of "CONNECT": HttpConnect
    of "TRACE": HttpTrace
    else: raise newException(ValueError, "Invalid HTTP method " & name)

proc processRequest(
  server: HttpServer,
  req: Request,
  client: Socket,
  address: string,
  lineFut: var string,
  callback: proc (request: Request): void {.closure, gcsafe.},
): bool =

  # Alias `request` to `req.mget()` so we don't have to write `mget` everywhere.
  template request(): Request =
    req

  # GET /path HTTP/1.1
  # Header: val
  # \n
  request.headers.clear()
  request.body = ""
  request.hostname.shallowCopy(address)
  assert client != nil
  request.client = client

  # We should skip at least one empty line before the request
  # https://tools.ietf.org/html/rfc7230#section-3.5
  for i in 0..1:
    lineFut.setLen(0)
    #client.recvLineInto(lineFut, maxLength=maxLine) # TODO: Timeouts.
    lineFut = client.recvLine(maxLength=maxLine)

    if lineFut == "":
      client.close()
      return false

    if lineFut.len > maxLine:
      request.respondError(Http413)
      client.close()
      return false
    if lineFut != "\c\L":
      break

  # First line - GET /path HTTP/1.1
  var i = 0
  for linePart in lineFut.split(' '):
    case i
    of 0:
      try:
        request.reqMethod = parseUppercaseMethod(linePart)
      except ValueError:
        request.respondError(Http400)
        return true # Retry processing of request
    of 1:
      try:
        parseUri(linePart, request.url)
      except ValueError:
        request.respondError(Http400)
        return true
    of 2:
      try:
        request.protocol = parseProtocol(linePart)
      except ValueError:
        request.respondError(Http400)
        return true
    else:
      request.respondError(Http400)
      return true
    inc i

  # Headers
  while true:
    i = 0
    lineFut.setLen(0)
    #client.recvLineInto(lineFut, maxLength=maxLine)
    lineFut = client.recvLine(maxLength=maxLine)

    if lineFut == "":
      client.close(); return false
    if lineFut.len > maxLine:
      request.respondError(Http413)
      client.close(); return false
    if lineFut == "\c\L": break
    let (key, value) = parseHeader(lineFut)
    request.headers[key] = value
    # Ensure the client isn't trying to DoS us.
    if request.headers.len > headerLimit:
      client.sendStatus("400 Bad Request")
      request.client.close()
      return false

  if request.reqMethod == HttpPost:
    # Check for Expect header
    if request.headers.hasKey("Expect"):
      if "100-continue" in request.headers["Expect"]:
        client.sendStatus("100 Continue")
      else:
        client.sendStatus("417 Expectation Failed")

  # Read the body
  # - Check for Content-length header
  if request.headers.hasKey("Content-Length"):
    var contentLength = 0
    if parseSaturatedNatural(request.headers["Content-Length"], contentLength) == 0:
      request.respond(Http400, "Bad Request. Invalid Content-Length.")
      return true
    else:
      if contentLength > server.maxBody:
        request.respondError(Http413)
        return false
      request.body = client.recv(contentLength)
      if request.body.len != contentLength:
        request.respond(Http400, "Bad Request. Content-Length does not match actual.")
        return true
  elif request.reqMethod == HttpPost:
    request.respond(Http411, "Content-Length required.")
    return true

  # Call the user's callback.
  callback(request)

  if "upgrade" in request.headers.getOrDefault("connection"):
    return false

  # The request has been served, from this point on returning `true` means the
  # connection will not be closed and will be kept in the connection pool.

  # Persistent connections
  if (request.protocol == HttpVer11 and
      cmpIgnoreCase(request.headers.getOrDefault("connection"), "close") != 0) or
     (request.protocol == HttpVer10 and
      cmpIgnoreCase(request.headers.getOrDefault("connection"), "keep-alive") == 0):
    # In HTTP 1.1 we assume that connection is persistent. Unless connection
    # header states otherwise.
    # In HTTP 1.0 we assume that the connection should not be persistent.
    # Unless the connection header states otherwise.
    return true
  else:
    request.client.close()
    return false

proc processClient(server: HttpServer, client: Socket, address: string,
                   callback: proc (request: Request):
                      void {.closure, gcsafe.}) =
  var request = new Request
  request.url = initUri()
  request.headers = newHttpHeaders()
  var lineFut = newStringOfCap(80)

  let retry = processRequest(
    server, request, client, address, lineFut, callback
  )

proc serve*(server: HttpServer, port: Port,
            callback: proc (request: Request): void {.closure,gcsafe.},
            address = "") =
  ## Starts the process of listening for incoming HTTP connections on the
  ## specified address and port.
  ##
  ## When a request is made by a client the specified callback will be called.
  server.socket = newSocket()
  if server.reuseAddr:
    server.socket.setSockOpt(OptReuseAddr, true)
  if server.reusePort:
    server.socket.setSockOpt(OptReusePort, true)
  server.socket.bindAddr(port, address)
  server.socket.listen()

  while true:
    var address: string
    var client: Socket
    server.socket.acceptAddr(client, address)
    processClient(server, client, address, callback)
    #echo(f.isNil)
    #echo(f.repr)

proc close*(server: HttpServer) =
  ## Terminates the async http server instance.
  server.socket.close()

when not defined(testing) and isMainModule:
  proc main =
    var server = newHttpServer()
    proc cb(req: Request) =
      #echo(req.reqMethod, " ", req.url)
      #echo(req.headers)
      let headers = {"Date": "Tue, 29 Apr 2014 23:40:08 GMT",
          "Content-type": "text/plain; charset=utf-8"}
      req.respond(Http200, "Hello World", headers.newHttpHeaders())

    server.serve(Port(5555), cb)
  main()
