# main block begin
{.hint[XDeclaredButNotUsed]:off.}
const NNG_MAJOR_VERSION* = 1
const NNG_MINOR_VERSION* = 1
const NNG_PATCH_VERSION* = 1
const NNG_RELEASE_SUFFIX* = ""
const NNG_OPT_SOCKNAME* = "socket-name"
const NNG_OPT_RAW* = "raw"
const NNG_OPT_PROTO* = "protocol"
const NNG_OPT_PROTONAME* = "protocol-name"
const NNG_OPT_PEER* = "peer"
const NNG_OPT_PEERNAME* = "peer-name"
const NNG_OPT_RECVBUF* = "recv-buffer"
const NNG_OPT_SENDBUF* = "send-buffer"
const NNG_OPT_RECVFD* = "recv-fd"
const NNG_OPT_SENDFD* = "send-fd"
const NNG_OPT_RECVTIMEO* = "recv-timeout"
const NNG_OPT_SENDTIMEO* = "send-timeout"
const NNG_OPT_LOCADDR* = "local-address"
const NNG_OPT_REMADDR* = "remote-address"
const NNG_OPT_URL* = "url"
const NNG_OPT_MAXTTL* = "ttl-max"
const NNG_OPT_RECVMAXSZ* = "recv-size-max"
const NNG_OPT_RECONNMINT* = "reconnect-time-min"
const NNG_OPT_RECONNMAXT* = "reconnect-time-max"
const NNG_OPT_TLS_CONFIG* = "tls-config"
const NNG_OPT_TLS_AUTH_MODE* = "tls-authmode"
const NNG_OPT_TLS_CERT_KEY_FILE* = "tls-cert-key-file"
const NNG_OPT_TLS_CA_FILE* = "tls-ca-file"
const NNG_OPT_TLS_SERVER_NAME* = "tls-server-name"
const NNG_OPT_TLS_VERIFIED* = "tls-verified"
const NNG_OPT_TCP_NODELAY* = "tcp-nodelay"
const NNG_OPT_TCP_KEEPALIVE* = "tcp-keepalive"
const NNG_OPT_TCP_BOUND_PORT* = "tcp-bound-port"
const NNG_OPT_IPC_SECURITY_DESCRIPTOR* = "ipc:security-descriptor"
const NNG_OPT_IPC_PERMISSIONS* = "ipc:permissions"
const NNG_OPT_IPC_PEER_UID* = "ipc:peer-uid"
const NNG_OPT_IPC_PEER_GID* = "ipc:peer-gid"
const NNG_OPT_IPC_PEER_PID* = "ipc:peer-pid"
const NNG_OPT_IPC_PEER_ZONEID* = "ipc:peer-zoneid"
const NNG_OPT_WS_REQUEST_HEADERS* = "ws:request-headers"
const NNG_OPT_WS_RESPONSE_HEADERS* = "ws:response-headers"
const NNG_OPT_WS_RESPONSE_HEADER* = "ws:response-header:"
const NNG_OPT_WS_REQUEST_HEADER* = "ws:request-header:"
const NNG_OPT_WS_REQUEST_URI* = "ws:request-uri"
const NNG_OPT_WS_SENDMAXFRAME* = "ws:txframe-max"
const NNG_OPT_WS_RECVMAXFRAME* = "ws:rxframe-max"
const NNG_OPT_WS_PROTOCOL* = "ws:protocol"
const NNG_OPT_REQ_RESENDTIME* = "req:resend-time"
const NNG_OPT_SUB_SUBSCRIBE* = "sub:subscribe"
const NNG_OPT_SUB_UNSUBSCRIBE* = "sub:unsubscribe"
# enum nng_sockaddr_family
const NNG_AF_UNSPEC* = 0
const NNG_AF_INPROC* = 1
const NNG_AF_IPC* = 2
const NNG_AF_INET* = 3
const NNG_AF_INET6* = 4
const NNG_AF_ZT* = 5
proc nng_sockaddr_family_tostr(v : int) : string =
  let iv = cast[int32](v)
  var ecname = "nng_sockaddr_family_unknown"
  if iv != iv: discard
  elif iv == NNG_AF_UNSPEC: ecname = "NNG_AF_UNSPEC"
  elif iv == NNG_AF_INPROC: ecname = "NNG_AF_INPROC"
  elif iv == NNG_AF_IPC: ecname = "NNG_AF_IPC"
  elif iv == NNG_AF_INET: ecname = "NNG_AF_INET"
  elif iv == NNG_AF_INET6: ecname = "NNG_AF_INET6"
  elif iv == NNG_AF_ZT: ecname = "NNG_AF_ZT"
  else: discard
  return ecname
#// void nng_fini()

proc nng_fini*()   {.importc:"nng_fini", header:"<nng/ponng.h>".}

type nng_socket* {.final, importc:"nng_socket", header:"<nng/ponng.h>".} = object

#// int nng_close(nng_socket)

proc nng_close*(a0 : nng_socket)  : int32 {.importc:"nng_close", header:"<nng/ponng.h>".}


#// int nng_socket_id(nng_socket)

proc nng_socket_id*(a0 : nng_socket)  : int32 {.importc:"nng_socket_id", header:"<nng/ponng.h>".}

#// void nng_closeall()

proc nng_closeall*()   {.importc:"nng_closeall", header:"<nng/ponng.h>".}


#// int nng_setopt(nng_socket, const char *, const void *, size_t)

proc nng_setopt*(a0 : nng_socket, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_setopt", header:"<nng/ponng.h>".}


#// int nng_setopt_bool(nng_socket, const char *, _Bool)

proc nng_setopt_bool*(a0 : nng_socket, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_setopt_bool", header:"<nng/ponng.h>".}


#// int nng_setopt_int(nng_socket, const char *, int)

proc nng_setopt_int*(a0 : nng_socket, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_setopt_int", header:"<nng/ponng.h>".}


type nng_duration* {.final, importc:"nng_duration", header:"<nng/ponng.h>".} = object

#// int nng_setopt_ms(nng_socket, const char *, nng_duration)

proc nng_setopt_ms*(a0 : nng_socket, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_setopt_ms", header:"<nng/ponng.h>".}


#// int nng_setopt_size(nng_socket, const char *, size_t)

proc nng_setopt_size*(a0 : nng_socket, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_setopt_size", header:"<nng/ponng.h>".}


#// int nng_setopt_uint64(nng_socket, const char *, uint64_t)

proc nng_setopt_uint64*(a0 : nng_socket, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_setopt_uint64", header:"<nng/ponng.h>".}


#// int nng_setopt_string(nng_socket, const char *, const char *)

proc nng_setopt_string*(a0 : nng_socket, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_setopt_string", header:"<nng/ponng.h>".}


#// int nng_setopt_ptr(nng_socket, const char *, void *)

proc nng_setopt_ptr*(a0 : nng_socket, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_setopt_ptr", header:"<nng/ponng.h>".}


#// int nng_getopt(nng_socket, const char *, void *, size_t *)

proc nng_getopt*(a0 : nng_socket, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_getopt", header:"<nng/ponng.h>".}


#// int nng_getopt_bool(nng_socket, const char *, _Bool *)

proc nng_getopt_bool*(a0 : nng_socket, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_getopt_bool", header:"<nng/ponng.h>".}


#// int nng_getopt_int(nng_socket, const char *, int *)

proc nng_getopt_int*(a0 : nng_socket, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_getopt_int", header:"<nng/ponng.h>".}



#// int nng_getopt_ms(nng_socket, const char *, nng_duration *)

proc nng_getopt_ms*(a0 : nng_socket, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_getopt_ms", header:"<nng/ponng.h>".}


#// int nng_getopt_size(nng_socket, const char *, size_t *)

proc nng_getopt_size*(a0 : nng_socket, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_getopt_size", header:"<nng/ponng.h>".}


#// int nng_getopt_uint64(nng_socket, const char *, uint64_t *)

proc nng_getopt_uint64*(a0 : nng_socket, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_getopt_uint64", header:"<nng/ponng.h>".}


#// int nng_getopt_ptr(nng_socket, const char *, void **)

proc nng_getopt_ptr*(a0 : nng_socket, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_getopt_ptr", header:"<nng/ponng.h>".}


#// int nng_getopt_string(nng_socket, const char *, char **)

proc nng_getopt_string*(a0 : nng_socket, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_getopt_string", header:"<nng/ponng.h>".}


#// int nng_socket_set(nng_socket, const char *, const void *, size_t)

proc nng_socket_set*(a0 : nng_socket, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_socket_set", header:"<nng/ponng.h>".}


#// int nng_socket_set_bool(nng_socket, const char *, _Bool)

proc nng_socket_set_bool*(a0 : nng_socket, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_socket_set_bool", header:"<nng/ponng.h>".}


#// int nng_socket_set_int(nng_socket, const char *, int)

proc nng_socket_set_int*(a0 : nng_socket, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_socket_set_int", header:"<nng/ponng.h>".}


#// int nng_socket_set_size(nng_socket, const char *, size_t)

proc nng_socket_set_size*(a0 : nng_socket, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_socket_set_size", header:"<nng/ponng.h>".}


#// int nng_socket_set_uint64(nng_socket, const char *, uint64_t)

proc nng_socket_set_uint64*(a0 : nng_socket, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_socket_set_uint64", header:"<nng/ponng.h>".}


#// int nng_socket_set_string(nng_socket, const char *, const char *)

proc nng_socket_set_string*(a0 : nng_socket, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_socket_set_string", header:"<nng/ponng.h>".}


#// int nng_socket_set_ptr(nng_socket, const char *, void *)

proc nng_socket_set_ptr*(a0 : nng_socket, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_socket_set_ptr", header:"<nng/ponng.h>".}



#// int nng_socket_set_ms(nng_socket, const char *, nng_duration)

proc nng_socket_set_ms*(a0 : nng_socket, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_socket_set_ms", header:"<nng/ponng.h>".}


type nng_sockaddr* {.final, importc:"nng_sockaddr", header:"<nng/ponng.h>".} = object

#// int nng_socket_set_addr(nng_socket, const char *, const nng_sockaddr *)

proc nng_socket_set_addr*(a0 : nng_socket, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_socket_set_addr", header:"<nng/ponng.h>".}


#// int nng_socket_get(nng_socket, const char *, void *, size_t *)

proc nng_socket_get*(a0 : nng_socket, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_socket_get", header:"<nng/ponng.h>".}


#// int nng_socket_get_bool(nng_socket, const char *, _Bool *)

proc nng_socket_get_bool*(a0 : nng_socket, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_socket_get_bool", header:"<nng/ponng.h>".}


#// int nng_socket_get_int(nng_socket, const char *, int *)

proc nng_socket_get_int*(a0 : nng_socket, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_socket_get_int", header:"<nng/ponng.h>".}


#// int nng_socket_get_size(nng_socket, const char *, size_t *)

proc nng_socket_get_size*(a0 : nng_socket, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_socket_get_size", header:"<nng/ponng.h>".}


#// int nng_socket_get_uint64(nng_socket, const char *, uint64_t *)

proc nng_socket_get_uint64*(a0 : nng_socket, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_socket_get_uint64", header:"<nng/ponng.h>".}


#// int nng_socket_get_string(nng_socket, const char *, char **)

proc nng_socket_get_string*(a0 : nng_socket, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_socket_get_string", header:"<nng/ponng.h>".}


#// int nng_socket_get_ptr(nng_socket, const char *, void **)

proc nng_socket_get_ptr*(a0 : nng_socket, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_socket_get_ptr", header:"<nng/ponng.h>".}



#// int nng_socket_get_ms(nng_socket, const char *, nng_duration *)

proc nng_socket_get_ms*(a0 : nng_socket, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_socket_get_ms", header:"<nng/ponng.h>".}



#// int nng_socket_get_addr(nng_socket, const char *, nng_sockaddr *)

proc nng_socket_get_addr*(a0 : nng_socket, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_socket_get_addr", header:"<nng/ponng.h>".}

# enum 
const NNG_PIPE_EV_ADD_PRE* = 0
const NNG_PIPE_EV_ADD_POST* = 1
const NNG_PIPE_EV_REM_POST* = 2
const NNG_PIPE_EV_NUM* = 3
proc NNG_PIPE_EV_enum_tostr(v : int) : string =
  let iv = cast[int32](v)
  var ecname = "NNG_PIPE_EV_enum_unknown"
  if iv != iv: discard
  elif iv == NNG_PIPE_EV_ADD_PRE: ecname = "NNG_PIPE_EV_ADD_PRE"
  elif iv == NNG_PIPE_EV_ADD_POST: ecname = "NNG_PIPE_EV_ADD_POST"
  elif iv == NNG_PIPE_EV_REM_POST: ecname = "NNG_PIPE_EV_REM_POST"
  elif iv == NNG_PIPE_EV_NUM: ecname = "NNG_PIPE_EV_NUM"
  else: discard
  return ecname

type nng_pipe_ev* {.final, importc:"nng_pipe_ev", header:"<nng/ponng.h>".} = object

type nng_pipe_cb* {.final, importc:"nng_pipe_cb", header:"<nng/ponng.h>".} = object

#// int nng_pipe_notify(nng_socket, nng_pipe_ev, nng_pipe_cb, void *)

proc nng_pipe_notify*(a0 : nng_socket, a1 : nng_pipe_ev, a2 : nng_pipe_cb, a3 : pointer)  : int32 {.importc:"nng_pipe_notify", header:"<nng/ponng.h>".}


type nng_listener* {.final, importc:"nng_listener", header:"<nng/ponng.h>".} = object

#// int nng_listen(nng_socket, const char *, nng_listener *, int)

proc nng_listen*(a0 : nng_socket, a1 : cstring, a2 : ptr nng_listener, a3 : int32)  : int32 {.importc:"nng_listen", header:"<nng/ponng.h>".}


type nng_dialer* {.final, importc:"nng_dialer", header:"<nng/ponng.h>".} = object

#// int nng_dial(nng_socket, const char *, nng_dialer *, int)

proc nng_dial*(a0 : nng_socket, a1 : cstring, a2 : ptr nng_dialer, a3 : int32)  : int32 {.importc:"nng_dial", header:"<nng/ponng.h>".}



#// int nng_dialer_create(nng_dialer *, nng_socket, const char *)

proc nng_dialer_create*(a0 : ptr nng_dialer, a1 : nng_socket, a2 : cstring)  : int32 {.importc:"nng_dialer_create", header:"<nng/ponng.h>".}



#// int nng_listener_create(nng_listener *, nng_socket, const char *)

proc nng_listener_create*(a0 : ptr nng_listener, a1 : nng_socket, a2 : cstring)  : int32 {.importc:"nng_listener_create", header:"<nng/ponng.h>".}


#// int nng_dialer_start(nng_dialer, int)

proc nng_dialer_start*(a0 : nng_dialer, a1 : int32)  : int32 {.importc:"nng_dialer_start", header:"<nng/ponng.h>".}


#// int nng_listener_start(nng_listener, int)

proc nng_listener_start*(a0 : nng_listener, a1 : int32)  : int32 {.importc:"nng_listener_start", header:"<nng/ponng.h>".}


#// int nng_dialer_close(nng_dialer)

proc nng_dialer_close*(a0 : nng_dialer)  : int32 {.importc:"nng_dialer_close", header:"<nng/ponng.h>".}


#// int nng_listener_close(nng_listener)

proc nng_listener_close*(a0 : nng_listener)  : int32 {.importc:"nng_listener_close", header:"<nng/ponng.h>".}


#// int nng_dialer_id(nng_dialer)

proc nng_dialer_id*(a0 : nng_dialer)  : int32 {.importc:"nng_dialer_id", header:"<nng/ponng.h>".}


#// int nng_listener_id(nng_listener)

proc nng_listener_id*(a0 : nng_listener)  : int32 {.importc:"nng_listener_id", header:"<nng/ponng.h>".}


#// int nng_dialer_setopt(nng_dialer, const char *, const void *, size_t)

proc nng_dialer_setopt*(a0 : nng_dialer, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_dialer_setopt", header:"<nng/ponng.h>".}


#// int nng_dialer_setopt_bool(nng_dialer, const char *, _Bool)

proc nng_dialer_setopt_bool*(a0 : nng_dialer, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_dialer_setopt_bool", header:"<nng/ponng.h>".}


#// int nng_dialer_setopt_int(nng_dialer, const char *, int)

proc nng_dialer_setopt_int*(a0 : nng_dialer, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_dialer_setopt_int", header:"<nng/ponng.h>".}



#// int nng_dialer_setopt_ms(nng_dialer, const char *, nng_duration)

proc nng_dialer_setopt_ms*(a0 : nng_dialer, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_dialer_setopt_ms", header:"<nng/ponng.h>".}


#// int nng_dialer_setopt_size(nng_dialer, const char *, size_t)

proc nng_dialer_setopt_size*(a0 : nng_dialer, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_dialer_setopt_size", header:"<nng/ponng.h>".}


#// int nng_dialer_setopt_uint64(nng_dialer, const char *, uint64_t)

proc nng_dialer_setopt_uint64*(a0 : nng_dialer, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_dialer_setopt_uint64", header:"<nng/ponng.h>".}


#// int nng_dialer_setopt_ptr(nng_dialer, const char *, void *)

proc nng_dialer_setopt_ptr*(a0 : nng_dialer, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_dialer_setopt_ptr", header:"<nng/ponng.h>".}


#// int nng_dialer_setopt_string(nng_dialer, const char *, const char *)

proc nng_dialer_setopt_string*(a0 : nng_dialer, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_dialer_setopt_string", header:"<nng/ponng.h>".}


#// int nng_dialer_getopt(nng_dialer, const char *, void *, size_t *)

proc nng_dialer_getopt*(a0 : nng_dialer, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_dialer_getopt", header:"<nng/ponng.h>".}


#// int nng_dialer_getopt_bool(nng_dialer, const char *, _Bool *)

proc nng_dialer_getopt_bool*(a0 : nng_dialer, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_dialer_getopt_bool", header:"<nng/ponng.h>".}


#// int nng_dialer_getopt_int(nng_dialer, const char *, int *)

proc nng_dialer_getopt_int*(a0 : nng_dialer, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_dialer_getopt_int", header:"<nng/ponng.h>".}



#// int nng_dialer_getopt_ms(nng_dialer, const char *, nng_duration *)

proc nng_dialer_getopt_ms*(a0 : nng_dialer, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_dialer_getopt_ms", header:"<nng/ponng.h>".}


#// int nng_dialer_getopt_size(nng_dialer, const char *, size_t *)

proc nng_dialer_getopt_size*(a0 : nng_dialer, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_dialer_getopt_size", header:"<nng/ponng.h>".}



#// int nng_dialer_getopt_sockaddr(nng_dialer, const char *, nng_sockaddr *)

proc nng_dialer_getopt_sockaddr*(a0 : nng_dialer, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_dialer_getopt_sockaddr", header:"<nng/ponng.h>".}


#// int nng_dialer_getopt_uint64(nng_dialer, const char *, uint64_t *)

proc nng_dialer_getopt_uint64*(a0 : nng_dialer, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_dialer_getopt_uint64", header:"<nng/ponng.h>".}


#// int nng_dialer_getopt_ptr(nng_dialer, const char *, void **)

proc nng_dialer_getopt_ptr*(a0 : nng_dialer, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_dialer_getopt_ptr", header:"<nng/ponng.h>".}


#// int nng_dialer_getopt_string(nng_dialer, const char *, char **)

proc nng_dialer_getopt_string*(a0 : nng_dialer, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_dialer_getopt_string", header:"<nng/ponng.h>".}


#// int nng_dialer_set(nng_dialer, const char *, const void *, size_t)

proc nng_dialer_set*(a0 : nng_dialer, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_dialer_set", header:"<nng/ponng.h>".}


#// int nng_dialer_set_bool(nng_dialer, const char *, _Bool)

proc nng_dialer_set_bool*(a0 : nng_dialer, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_dialer_set_bool", header:"<nng/ponng.h>".}


#// int nng_dialer_set_int(nng_dialer, const char *, int)

proc nng_dialer_set_int*(a0 : nng_dialer, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_dialer_set_int", header:"<nng/ponng.h>".}


#// int nng_dialer_set_size(nng_dialer, const char *, size_t)

proc nng_dialer_set_size*(a0 : nng_dialer, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_dialer_set_size", header:"<nng/ponng.h>".}


#// int nng_dialer_set_uint64(nng_dialer, const char *, uint64_t)

proc nng_dialer_set_uint64*(a0 : nng_dialer, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_dialer_set_uint64", header:"<nng/ponng.h>".}


#// int nng_dialer_set_string(nng_dialer, const char *, const char *)

proc nng_dialer_set_string*(a0 : nng_dialer, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_dialer_set_string", header:"<nng/ponng.h>".}


#// int nng_dialer_set_ptr(nng_dialer, const char *, void *)

proc nng_dialer_set_ptr*(a0 : nng_dialer, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_dialer_set_ptr", header:"<nng/ponng.h>".}



#// int nng_dialer_set_ms(nng_dialer, const char *, nng_duration)

proc nng_dialer_set_ms*(a0 : nng_dialer, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_dialer_set_ms", header:"<nng/ponng.h>".}



#// int nng_dialer_set_addr(nng_dialer, const char *, const nng_sockaddr *)

proc nng_dialer_set_addr*(a0 : nng_dialer, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_dialer_set_addr", header:"<nng/ponng.h>".}


#// int nng_dialer_get(nng_dialer, const char *, void *, size_t *)

proc nng_dialer_get*(a0 : nng_dialer, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_dialer_get", header:"<nng/ponng.h>".}


#// int nng_dialer_get_bool(nng_dialer, const char *, _Bool *)

proc nng_dialer_get_bool*(a0 : nng_dialer, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_dialer_get_bool", header:"<nng/ponng.h>".}


#// int nng_dialer_get_int(nng_dialer, const char *, int *)

proc nng_dialer_get_int*(a0 : nng_dialer, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_dialer_get_int", header:"<nng/ponng.h>".}


#// int nng_dialer_get_size(nng_dialer, const char *, size_t *)

proc nng_dialer_get_size*(a0 : nng_dialer, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_dialer_get_size", header:"<nng/ponng.h>".}


#// int nng_dialer_get_uint64(nng_dialer, const char *, uint64_t *)

proc nng_dialer_get_uint64*(a0 : nng_dialer, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_dialer_get_uint64", header:"<nng/ponng.h>".}


#// int nng_dialer_get_string(nng_dialer, const char *, char **)

proc nng_dialer_get_string*(a0 : nng_dialer, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_dialer_get_string", header:"<nng/ponng.h>".}


#// int nng_dialer_get_ptr(nng_dialer, const char *, void **)

proc nng_dialer_get_ptr*(a0 : nng_dialer, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_dialer_get_ptr", header:"<nng/ponng.h>".}



#// int nng_dialer_get_ms(nng_dialer, const char *, nng_duration *)

proc nng_dialer_get_ms*(a0 : nng_dialer, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_dialer_get_ms", header:"<nng/ponng.h>".}



#// int nng_dialer_get_addr(nng_dialer, const char *, nng_sockaddr *)

proc nng_dialer_get_addr*(a0 : nng_dialer, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_dialer_get_addr", header:"<nng/ponng.h>".}


#// int nng_listener_setopt(nng_listener, const char *, const void *, size_t)

proc nng_listener_setopt*(a0 : nng_listener, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_listener_setopt", header:"<nng/ponng.h>".}


#// int nng_listener_setopt_bool(nng_listener, const char *, _Bool)

proc nng_listener_setopt_bool*(a0 : nng_listener, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_listener_setopt_bool", header:"<nng/ponng.h>".}


#// int nng_listener_setopt_int(nng_listener, const char *, int)

proc nng_listener_setopt_int*(a0 : nng_listener, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_listener_setopt_int", header:"<nng/ponng.h>".}



#// int nng_listener_setopt_ms(nng_listener, const char *, nng_duration)

proc nng_listener_setopt_ms*(a0 : nng_listener, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_listener_setopt_ms", header:"<nng/ponng.h>".}


#// int nng_listener_setopt_size(nng_listener, const char *, size_t)

proc nng_listener_setopt_size*(a0 : nng_listener, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_listener_setopt_size", header:"<nng/ponng.h>".}


#// int nng_listener_setopt_uint64(nng_listener, const char *, uint64_t)

proc nng_listener_setopt_uint64*(a0 : nng_listener, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_listener_setopt_uint64", header:"<nng/ponng.h>".}


#// int nng_listener_setopt_ptr(nng_listener, const char *, void *)

proc nng_listener_setopt_ptr*(a0 : nng_listener, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_listener_setopt_ptr", header:"<nng/ponng.h>".}


#// int nng_listener_setopt_string(nng_listener, const char *, const char *)

proc nng_listener_setopt_string*(a0 : nng_listener, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_listener_setopt_string", header:"<nng/ponng.h>".}


#// int nng_listener_getopt(nng_listener, const char *, void *, size_t *)

proc nng_listener_getopt*(a0 : nng_listener, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_listener_getopt", header:"<nng/ponng.h>".}


#// int nng_listener_getopt_bool(nng_listener, const char *, _Bool *)

proc nng_listener_getopt_bool*(a0 : nng_listener, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_listener_getopt_bool", header:"<nng/ponng.h>".}


#// int nng_listener_getopt_int(nng_listener, const char *, int *)

proc nng_listener_getopt_int*(a0 : nng_listener, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_listener_getopt_int", header:"<nng/ponng.h>".}



#// int nng_listener_getopt_ms(nng_listener, const char *, nng_duration *)

proc nng_listener_getopt_ms*(a0 : nng_listener, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_listener_getopt_ms", header:"<nng/ponng.h>".}


#// int nng_listener_getopt_size(nng_listener, const char *, size_t *)

proc nng_listener_getopt_size*(a0 : nng_listener, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_listener_getopt_size", header:"<nng/ponng.h>".}



#// int nng_listener_getopt_sockaddr(nng_listener, const char *, nng_sockaddr *)

proc nng_listener_getopt_sockaddr*(a0 : nng_listener, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_listener_getopt_sockaddr", header:"<nng/ponng.h>".}


#// int nng_listener_getopt_uint64(nng_listener, const char *, uint64_t *)

proc nng_listener_getopt_uint64*(a0 : nng_listener, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_listener_getopt_uint64", header:"<nng/ponng.h>".}


#// int nng_listener_getopt_ptr(nng_listener, const char *, void **)

proc nng_listener_getopt_ptr*(a0 : nng_listener, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_listener_getopt_ptr", header:"<nng/ponng.h>".}


#// int nng_listener_getopt_string(nng_listener, const char *, char **)

proc nng_listener_getopt_string*(a0 : nng_listener, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_listener_getopt_string", header:"<nng/ponng.h>".}


#// int nng_listener_set(nng_listener, const char *, const void *, size_t)

proc nng_listener_set*(a0 : nng_listener, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_listener_set", header:"<nng/ponng.h>".}


#// int nng_listener_set_bool(nng_listener, const char *, _Bool)

proc nng_listener_set_bool*(a0 : nng_listener, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_listener_set_bool", header:"<nng/ponng.h>".}


#// int nng_listener_set_int(nng_listener, const char *, int)

proc nng_listener_set_int*(a0 : nng_listener, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_listener_set_int", header:"<nng/ponng.h>".}


#// int nng_listener_set_size(nng_listener, const char *, size_t)

proc nng_listener_set_size*(a0 : nng_listener, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_listener_set_size", header:"<nng/ponng.h>".}


#// int nng_listener_set_uint64(nng_listener, const char *, uint64_t)

proc nng_listener_set_uint64*(a0 : nng_listener, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_listener_set_uint64", header:"<nng/ponng.h>".}


#// int nng_listener_set_string(nng_listener, const char *, const char *)

proc nng_listener_set_string*(a0 : nng_listener, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_listener_set_string", header:"<nng/ponng.h>".}


#// int nng_listener_set_ptr(nng_listener, const char *, void *)

proc nng_listener_set_ptr*(a0 : nng_listener, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_listener_set_ptr", header:"<nng/ponng.h>".}



#// int nng_listener_set_ms(nng_listener, const char *, nng_duration)

proc nng_listener_set_ms*(a0 : nng_listener, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_listener_set_ms", header:"<nng/ponng.h>".}



#// int nng_listener_set_addr(nng_listener, const char *, const nng_sockaddr *)

proc nng_listener_set_addr*(a0 : nng_listener, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_listener_set_addr", header:"<nng/ponng.h>".}


#// int nng_listener_get(nng_listener, const char *, void *, size_t *)

proc nng_listener_get*(a0 : nng_listener, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_listener_get", header:"<nng/ponng.h>".}


#// int nng_listener_get_bool(nng_listener, const char *, _Bool *)

proc nng_listener_get_bool*(a0 : nng_listener, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_listener_get_bool", header:"<nng/ponng.h>".}


#// int nng_listener_get_int(nng_listener, const char *, int *)

proc nng_listener_get_int*(a0 : nng_listener, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_listener_get_int", header:"<nng/ponng.h>".}


#// int nng_listener_get_size(nng_listener, const char *, size_t *)

proc nng_listener_get_size*(a0 : nng_listener, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_listener_get_size", header:"<nng/ponng.h>".}


#// int nng_listener_get_uint64(nng_listener, const char *, uint64_t *)

proc nng_listener_get_uint64*(a0 : nng_listener, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_listener_get_uint64", header:"<nng/ponng.h>".}


#// int nng_listener_get_string(nng_listener, const char *, char **)

proc nng_listener_get_string*(a0 : nng_listener, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_listener_get_string", header:"<nng/ponng.h>".}


#// int nng_listener_get_ptr(nng_listener, const char *, void **)

proc nng_listener_get_ptr*(a0 : nng_listener, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_listener_get_ptr", header:"<nng/ponng.h>".}



#// int nng_listener_get_ms(nng_listener, const char *, nng_duration *)

proc nng_listener_get_ms*(a0 : nng_listener, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_listener_get_ms", header:"<nng/ponng.h>".}



#// int nng_listener_get_addr(nng_listener, const char *, nng_sockaddr *)

proc nng_listener_get_addr*(a0 : nng_listener, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_listener_get_addr", header:"<nng/ponng.h>".}

#// const char * nng_strerror(int)

proc nng_strerror*(a0 : int32)  : cstring {.importc:"nng_strerror", header:"<nng/ponng.h>".}


#// int nng_send(nng_socket, void *, size_t, int)

proc nng_send*(a0 : nng_socket, a1 : pointer, a2 : uint64, a3 : int32)  : int32 {.importc:"nng_send", header:"<nng/ponng.h>".}


#// int nng_recv(nng_socket, void *, size_t *, int)

proc nng_recv*(a0 : nng_socket, a1 : pointer, a2 : ptr uint64, a3 : int32)  : int32 {.importc:"nng_recv", header:"<nng/ponng.h>".}


type nng_msg* {.final, importc:"nng_msg", header:"<nng/ponng.h>".} = object

#// int nng_sendmsg(nng_socket, nng_msg *, int)

proc nng_sendmsg*(a0 : nng_socket, a1 : ptr nng_msg, a2 : int32)  : int32 {.importc:"nng_sendmsg", header:"<nng/ponng.h>".}



#// int nng_recvmsg(nng_socket, nng_msg **, int)

proc nng_recvmsg*(a0 : nng_socket, a1 : ptr ptr nng_msg, a2 : int32)  : int32 {.importc:"nng_recvmsg", header:"<nng/ponng.h>".}


type nng_aio* {.final, importc:"nng_aio", header:"<nng/ponng.h>".} = object

#// void nng_send_aio(nng_socket, nng_aio *)

proc nng_send_aio*(a0 : nng_socket, a1 : ptr nng_aio)   {.importc:"nng_send_aio", header:"<nng/ponng.h>".}



#// void nng_recv_aio(nng_socket, nng_aio *)

proc nng_recv_aio*(a0 : nng_socket, a1 : ptr nng_aio)   {.importc:"nng_recv_aio", header:"<nng/ponng.h>".}

type nng_ctx* {.final, importc:"nng_ctx", header:"<nng/ponng.h>".} = object


#// int nng_ctx_open(nng_ctx *, nng_socket)

proc nng_ctx_open*(a0 : ptr nng_ctx, a1 : nng_socket)  : int32 {.importc:"nng_ctx_open", header:"<nng/ponng.h>".}


#// int nng_ctx_close(nng_ctx)

proc nng_ctx_close*(a0 : nng_ctx)  : int32 {.importc:"nng_ctx_close", header:"<nng/ponng.h>".}


#// int nng_ctx_id(nng_ctx)

proc nng_ctx_id*(a0 : nng_ctx)  : int32 {.importc:"nng_ctx_id", header:"<nng/ponng.h>".}



#// void nng_ctx_recv(nng_ctx, nng_aio *)

proc nng_ctx_recv*(a0 : nng_ctx, a1 : ptr nng_aio)   {.importc:"nng_ctx_recv", header:"<nng/ponng.h>".}



#// void nng_ctx_send(nng_ctx, nng_aio *)

proc nng_ctx_send*(a0 : nng_ctx, a1 : ptr nng_aio)   {.importc:"nng_ctx_send", header:"<nng/ponng.h>".}


#// int nng_ctx_getopt(nng_ctx, const char *, void *, size_t *)

proc nng_ctx_getopt*(a0 : nng_ctx, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_ctx_getopt", header:"<nng/ponng.h>".}


#// int nng_ctx_getopt_bool(nng_ctx, const char *, _Bool *)

proc nng_ctx_getopt_bool*(a0 : nng_ctx, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_ctx_getopt_bool", header:"<nng/ponng.h>".}


#// int nng_ctx_getopt_int(nng_ctx, const char *, int *)

proc nng_ctx_getopt_int*(a0 : nng_ctx, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_ctx_getopt_int", header:"<nng/ponng.h>".}



#// int nng_ctx_getopt_ms(nng_ctx, const char *, nng_duration *)

proc nng_ctx_getopt_ms*(a0 : nng_ctx, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_ctx_getopt_ms", header:"<nng/ponng.h>".}


#// int nng_ctx_getopt_size(nng_ctx, const char *, size_t *)

proc nng_ctx_getopt_size*(a0 : nng_ctx, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_ctx_getopt_size", header:"<nng/ponng.h>".}


#// int nng_ctx_setopt(nng_ctx, const char *, const void *, size_t)

proc nng_ctx_setopt*(a0 : nng_ctx, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_ctx_setopt", header:"<nng/ponng.h>".}


#// int nng_ctx_setopt_bool(nng_ctx, const char *, _Bool)

proc nng_ctx_setopt_bool*(a0 : nng_ctx, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_ctx_setopt_bool", header:"<nng/ponng.h>".}


#// int nng_ctx_setopt_int(nng_ctx, const char *, int)

proc nng_ctx_setopt_int*(a0 : nng_ctx, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_ctx_setopt_int", header:"<nng/ponng.h>".}



#// int nng_ctx_setopt_ms(nng_ctx, const char *, nng_duration)

proc nng_ctx_setopt_ms*(a0 : nng_ctx, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_ctx_setopt_ms", header:"<nng/ponng.h>".}


#// int nng_ctx_setopt_size(nng_ctx, const char *, size_t)

proc nng_ctx_setopt_size*(a0 : nng_ctx, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_ctx_setopt_size", header:"<nng/ponng.h>".}


#// int nng_ctx_get(nng_ctx, const char *, void *, size_t *)

proc nng_ctx_get*(a0 : nng_ctx, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_ctx_get", header:"<nng/ponng.h>".}


#// int nng_ctx_get_bool(nng_ctx, const char *, _Bool *)

proc nng_ctx_get_bool*(a0 : nng_ctx, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_ctx_get_bool", header:"<nng/ponng.h>".}


#// int nng_ctx_get_int(nng_ctx, const char *, int *)

proc nng_ctx_get_int*(a0 : nng_ctx, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_ctx_get_int", header:"<nng/ponng.h>".}


#// int nng_ctx_get_size(nng_ctx, const char *, size_t *)

proc nng_ctx_get_size*(a0 : nng_ctx, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_ctx_get_size", header:"<nng/ponng.h>".}


#// int nng_ctx_get_uint64(nng_ctx, const char *, uint64_t *)

proc nng_ctx_get_uint64*(a0 : nng_ctx, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_ctx_get_uint64", header:"<nng/ponng.h>".}


#// int nng_ctx_get_string(nng_ctx, const char *, char **)

proc nng_ctx_get_string*(a0 : nng_ctx, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_ctx_get_string", header:"<nng/ponng.h>".}


#// int nng_ctx_get_ptr(nng_ctx, const char *, void **)

proc nng_ctx_get_ptr*(a0 : nng_ctx, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_ctx_get_ptr", header:"<nng/ponng.h>".}



#// int nng_ctx_get_ms(nng_ctx, const char *, nng_duration *)

proc nng_ctx_get_ms*(a0 : nng_ctx, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_ctx_get_ms", header:"<nng/ponng.h>".}



#// int nng_ctx_get_addr(nng_ctx, const char *, nng_sockaddr *)

proc nng_ctx_get_addr*(a0 : nng_ctx, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_ctx_get_addr", header:"<nng/ponng.h>".}


#// int nng_ctx_set(nng_ctx, const char *, const void *, size_t)

proc nng_ctx_set*(a0 : nng_ctx, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_ctx_set", header:"<nng/ponng.h>".}


#// int nng_ctx_set_bool(nng_ctx, const char *, _Bool)

proc nng_ctx_set_bool*(a0 : nng_ctx, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_ctx_set_bool", header:"<nng/ponng.h>".}


#// int nng_ctx_set_int(nng_ctx, const char *, int)

proc nng_ctx_set_int*(a0 : nng_ctx, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_ctx_set_int", header:"<nng/ponng.h>".}


#// int nng_ctx_set_size(nng_ctx, const char *, size_t)

proc nng_ctx_set_size*(a0 : nng_ctx, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_ctx_set_size", header:"<nng/ponng.h>".}


#// int nng_ctx_set_uint64(nng_ctx, const char *, uint64_t)

proc nng_ctx_set_uint64*(a0 : nng_ctx, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_ctx_set_uint64", header:"<nng/ponng.h>".}


#// int nng_ctx_set_string(nng_ctx, const char *, const char *)

proc nng_ctx_set_string*(a0 : nng_ctx, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_ctx_set_string", header:"<nng/ponng.h>".}


#// int nng_ctx_set_ptr(nng_ctx, const char *, void *)

proc nng_ctx_set_ptr*(a0 : nng_ctx, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_ctx_set_ptr", header:"<nng/ponng.h>".}



#// int nng_ctx_set_ms(nng_ctx, const char *, nng_duration)

proc nng_ctx_set_ms*(a0 : nng_ctx, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_ctx_set_ms", header:"<nng/ponng.h>".}



#// int nng_ctx_set_addr(nng_ctx, const char *, const nng_sockaddr *)

proc nng_ctx_set_addr*(a0 : nng_ctx, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_ctx_set_addr", header:"<nng/ponng.h>".}

#// void * nng_alloc(size_t)

proc nng_alloc*(a0 : uint64)  : pointer {.importc:"nng_alloc", header:"<nng/ponng.h>".}

#// void nng_free(void *, size_t)

proc nng_free*(a0 : pointer, a1 : uint64)   {.importc:"nng_free", header:"<nng/ponng.h>".}

#// char * nng_strdup(const char *)

proc nng_strdup*(a0 : cstring)  : ptr uint8 {.importc:"nng_strdup", header:"<nng/ponng.h>".}

#// void nng_strfree(char *)

proc nng_strfree*(a0 : ptr uint8)   {.importc:"nng_strfree", header:"<nng/ponng.h>".}


#// int nng_aio_alloc(nng_aio **, void (*)(void *), void *)

proc nng_aio_alloc*(a0 : ptr ptr nng_aio, a1 : pointer, a2 : pointer)  : int32 {.importc:"nng_aio_alloc", header:"<nng/ponng.h>".}


#// void nng_aio_free(nng_aio *)

proc nng_aio_free*(a0 : ptr nng_aio)   {.importc:"nng_aio_free", header:"<nng/ponng.h>".}


#// void nng_aio_stop(nng_aio *)

proc nng_aio_stop*(a0 : ptr nng_aio)   {.importc:"nng_aio_stop", header:"<nng/ponng.h>".}


#// int nng_aio_result(nng_aio *)

proc nng_aio_result*(a0 : ptr nng_aio)  : int32 {.importc:"nng_aio_result", header:"<nng/ponng.h>".}


#// size_t nng_aio_count(nng_aio *)

proc nng_aio_count*(a0 : ptr nng_aio)  : uint64 {.importc:"nng_aio_count", header:"<nng/ponng.h>".}


#// void nng_aio_cancel(nng_aio *)

proc nng_aio_cancel*(a0 : ptr nng_aio)   {.importc:"nng_aio_cancel", header:"<nng/ponng.h>".}


#// void nng_aio_abort(nng_aio *, int)

proc nng_aio_abort*(a0 : ptr nng_aio, a1 : int32)   {.importc:"nng_aio_abort", header:"<nng/ponng.h>".}


#// void nng_aio_wait(nng_aio *)

proc nng_aio_wait*(a0 : ptr nng_aio)   {.importc:"nng_aio_wait", header:"<nng/ponng.h>".}



#// void nng_aio_set_msg(nng_aio *, nng_msg *)

proc nng_aio_set_msg*(a0 : ptr nng_aio, a1 : ptr nng_msg)   {.importc:"nng_aio_set_msg", header:"<nng/ponng.h>".}



#// nng_msg * nng_aio_get_msg(nng_aio *)

proc nng_aio_get_msg*(a0 : ptr nng_aio)  : ptr nng_msg {.importc:"nng_aio_get_msg", header:"<nng/ponng.h>".}


#// int nng_aio_set_input(nng_aio *, unsigned int, void *)

proc nng_aio_set_input*(a0 : ptr nng_aio, a1 : uint32, a2 : pointer)  : int32 {.importc:"nng_aio_set_input", header:"<nng/ponng.h>".}


#// void * nng_aio_get_input(nng_aio *, unsigned int)

proc nng_aio_get_input*(a0 : ptr nng_aio, a1 : uint32)  : pointer {.importc:"nng_aio_get_input", header:"<nng/ponng.h>".}


#// int nng_aio_set_output(nng_aio *, unsigned int, void *)

proc nng_aio_set_output*(a0 : ptr nng_aio, a1 : uint32, a2 : pointer)  : int32 {.importc:"nng_aio_set_output", header:"<nng/ponng.h>".}


#// void * nng_aio_get_output(nng_aio *, unsigned int)

proc nng_aio_get_output*(a0 : ptr nng_aio, a1 : uint32)  : pointer {.importc:"nng_aio_get_output", header:"<nng/ponng.h>".}



#// void nng_aio_set_timeout(nng_aio *, nng_duration)

proc nng_aio_set_timeout*(a0 : ptr nng_aio, a1 : nng_duration)   {.importc:"nng_aio_set_timeout", header:"<nng/ponng.h>".}


type nng_iov* {.final, importc:"nng_iov", header:"<nng/ponng.h>".} = object

#// int nng_aio_set_iov(nng_aio *, unsigned int, const nng_iov *)

proc nng_aio_set_iov*(a0 : ptr nng_aio, a1 : uint32, a2 : ptr nng_iov)  : int32 {.importc:"nng_aio_set_iov", header:"<nng/ponng.h>".}


#// _Bool nng_aio_begin(nng_aio *)

proc nng_aio_begin*(a0 : ptr nng_aio)  : bool {.importc:"nng_aio_begin", header:"<nng/ponng.h>".}


#// void nng_aio_finish(nng_aio *, int)

proc nng_aio_finish*(a0 : ptr nng_aio, a1 : int32)   {.importc:"nng_aio_finish", header:"<nng/ponng.h>".}


type nng_aio_cancelfn* {.final, importc:"nng_aio_cancelfn", header:"<nng/ponng.h>".} = object

#// void nng_aio_defer(nng_aio *, nng_aio_cancelfn, void *)

proc nng_aio_defer*(a0 : ptr nng_aio, a1 : nng_aio_cancelfn, a2 : pointer)   {.importc:"nng_aio_defer", header:"<nng/ponng.h>".}



#// void nng_sleep_aio(nng_duration, nng_aio *)

proc nng_sleep_aio*(a0 : nng_duration, a1 : ptr nng_aio)   {.importc:"nng_sleep_aio", header:"<nng/ponng.h>".}


#// int nng_msg_alloc(nng_msg **, size_t)

proc nng_msg_alloc*(a0 : ptr ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_alloc", header:"<nng/ponng.h>".}


#// void nng_msg_free(nng_msg *)

proc nng_msg_free*(a0 : ptr nng_msg)   {.importc:"nng_msg_free", header:"<nng/ponng.h>".}


#// int nng_msg_realloc(nng_msg *, size_t)

proc nng_msg_realloc*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_realloc", header:"<nng/ponng.h>".}


#// void * nng_msg_header(nng_msg *)

proc nng_msg_header*(a0 : ptr nng_msg)  : pointer {.importc:"nng_msg_header", header:"<nng/ponng.h>".}


#// size_t nng_msg_header_len(const nng_msg *)

proc nng_msg_header_len*(a0 : ptr nng_msg)  : uint64 {.importc:"nng_msg_header_len", header:"<nng/ponng.h>".}


#// void * nng_msg_body(nng_msg *)

proc nng_msg_body*(a0 : ptr nng_msg)  : pointer {.importc:"nng_msg_body", header:"<nng/ponng.h>".}


#// size_t nng_msg_len(const nng_msg *)

proc nng_msg_len*(a0 : ptr nng_msg)  : uint64 {.importc:"nng_msg_len", header:"<nng/ponng.h>".}


#// int nng_msg_append(nng_msg *, const void *, size_t)

proc nng_msg_append*(a0 : ptr nng_msg, a1 : pointer, a2 : uint64)  : int32 {.importc:"nng_msg_append", header:"<nng/ponng.h>".}


#// int nng_msg_insert(nng_msg *, const void *, size_t)

proc nng_msg_insert*(a0 : ptr nng_msg, a1 : pointer, a2 : uint64)  : int32 {.importc:"nng_msg_insert", header:"<nng/ponng.h>".}


#// int nng_msg_trim(nng_msg *, size_t)

proc nng_msg_trim*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_trim", header:"<nng/ponng.h>".}


#// int nng_msg_chop(nng_msg *, size_t)

proc nng_msg_chop*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_chop", header:"<nng/ponng.h>".}


#// int nng_msg_header_append(nng_msg *, const void *, size_t)

proc nng_msg_header_append*(a0 : ptr nng_msg, a1 : pointer, a2 : uint64)  : int32 {.importc:"nng_msg_header_append", header:"<nng/ponng.h>".}


#// int nng_msg_header_insert(nng_msg *, const void *, size_t)

proc nng_msg_header_insert*(a0 : ptr nng_msg, a1 : pointer, a2 : uint64)  : int32 {.importc:"nng_msg_header_insert", header:"<nng/ponng.h>".}


#// int nng_msg_header_trim(nng_msg *, size_t)

proc nng_msg_header_trim*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_header_trim", header:"<nng/ponng.h>".}


#// int nng_msg_header_chop(nng_msg *, size_t)

proc nng_msg_header_chop*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_header_chop", header:"<nng/ponng.h>".}


#// int nng_msg_header_append_u16(nng_msg *, uint16_t)

proc nng_msg_header_append_u16*(a0 : ptr nng_msg, a1 : uint16)  : int32 {.importc:"nng_msg_header_append_u16", header:"<nng/ponng.h>".}


#// int nng_msg_header_append_u32(nng_msg *, uint32_t)

proc nng_msg_header_append_u32*(a0 : ptr nng_msg, a1 : uint32)  : int32 {.importc:"nng_msg_header_append_u32", header:"<nng/ponng.h>".}


#// int nng_msg_header_append_u64(nng_msg *, uint64_t)

proc nng_msg_header_append_u64*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_header_append_u64", header:"<nng/ponng.h>".}


#// int nng_msg_header_insert_u16(nng_msg *, uint16_t)

proc nng_msg_header_insert_u16*(a0 : ptr nng_msg, a1 : uint16)  : int32 {.importc:"nng_msg_header_insert_u16", header:"<nng/ponng.h>".}


#// int nng_msg_header_insert_u32(nng_msg *, uint32_t)

proc nng_msg_header_insert_u32*(a0 : ptr nng_msg, a1 : uint32)  : int32 {.importc:"nng_msg_header_insert_u32", header:"<nng/ponng.h>".}


#// int nng_msg_header_insert_u64(nng_msg *, uint64_t)

proc nng_msg_header_insert_u64*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_header_insert_u64", header:"<nng/ponng.h>".}


#// int nng_msg_header_chop_u16(nng_msg *, uint16_t *)

proc nng_msg_header_chop_u16*(a0 : ptr nng_msg, a1 : ptr uint16)  : int32 {.importc:"nng_msg_header_chop_u16", header:"<nng/ponng.h>".}


#// int nng_msg_header_chop_u32(nng_msg *, uint32_t *)

proc nng_msg_header_chop_u32*(a0 : ptr nng_msg, a1 : ptr uint32)  : int32 {.importc:"nng_msg_header_chop_u32", header:"<nng/ponng.h>".}


#// int nng_msg_header_chop_u64(nng_msg *, uint64_t *)

proc nng_msg_header_chop_u64*(a0 : ptr nng_msg, a1 : ptr uint64)  : int32 {.importc:"nng_msg_header_chop_u64", header:"<nng/ponng.h>".}


#// int nng_msg_header_trim_u16(nng_msg *, uint16_t *)

proc nng_msg_header_trim_u16*(a0 : ptr nng_msg, a1 : ptr uint16)  : int32 {.importc:"nng_msg_header_trim_u16", header:"<nng/ponng.h>".}


#// int nng_msg_header_trim_u32(nng_msg *, uint32_t *)

proc nng_msg_header_trim_u32*(a0 : ptr nng_msg, a1 : ptr uint32)  : int32 {.importc:"nng_msg_header_trim_u32", header:"<nng/ponng.h>".}


#// int nng_msg_header_trim_u64(nng_msg *, uint64_t *)

proc nng_msg_header_trim_u64*(a0 : ptr nng_msg, a1 : ptr uint64)  : int32 {.importc:"nng_msg_header_trim_u64", header:"<nng/ponng.h>".}


#// int nng_msg_append_u16(nng_msg *, uint16_t)

proc nng_msg_append_u16*(a0 : ptr nng_msg, a1 : uint16)  : int32 {.importc:"nng_msg_append_u16", header:"<nng/ponng.h>".}


#// int nng_msg_append_u32(nng_msg *, uint32_t)

proc nng_msg_append_u32*(a0 : ptr nng_msg, a1 : uint32)  : int32 {.importc:"nng_msg_append_u32", header:"<nng/ponng.h>".}


#// int nng_msg_append_u64(nng_msg *, uint64_t)

proc nng_msg_append_u64*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_append_u64", header:"<nng/ponng.h>".}


#// int nng_msg_insert_u16(nng_msg *, uint16_t)

proc nng_msg_insert_u16*(a0 : ptr nng_msg, a1 : uint16)  : int32 {.importc:"nng_msg_insert_u16", header:"<nng/ponng.h>".}


#// int nng_msg_insert_u32(nng_msg *, uint32_t)

proc nng_msg_insert_u32*(a0 : ptr nng_msg, a1 : uint32)  : int32 {.importc:"nng_msg_insert_u32", header:"<nng/ponng.h>".}


#// int nng_msg_insert_u64(nng_msg *, uint64_t)

proc nng_msg_insert_u64*(a0 : ptr nng_msg, a1 : uint64)  : int32 {.importc:"nng_msg_insert_u64", header:"<nng/ponng.h>".}


#// int nng_msg_chop_u16(nng_msg *, uint16_t *)

proc nng_msg_chop_u16*(a0 : ptr nng_msg, a1 : ptr uint16)  : int32 {.importc:"nng_msg_chop_u16", header:"<nng/ponng.h>".}


#// int nng_msg_chop_u32(nng_msg *, uint32_t *)

proc nng_msg_chop_u32*(a0 : ptr nng_msg, a1 : ptr uint32)  : int32 {.importc:"nng_msg_chop_u32", header:"<nng/ponng.h>".}


#// int nng_msg_chop_u64(nng_msg *, uint64_t *)

proc nng_msg_chop_u64*(a0 : ptr nng_msg, a1 : ptr uint64)  : int32 {.importc:"nng_msg_chop_u64", header:"<nng/ponng.h>".}


#// int nng_msg_trim_u16(nng_msg *, uint16_t *)

proc nng_msg_trim_u16*(a0 : ptr nng_msg, a1 : ptr uint16)  : int32 {.importc:"nng_msg_trim_u16", header:"<nng/ponng.h>".}


#// int nng_msg_trim_u32(nng_msg *, uint32_t *)

proc nng_msg_trim_u32*(a0 : ptr nng_msg, a1 : ptr uint32)  : int32 {.importc:"nng_msg_trim_u32", header:"<nng/ponng.h>".}


#// int nng_msg_trim_u64(nng_msg *, uint64_t *)

proc nng_msg_trim_u64*(a0 : ptr nng_msg, a1 : ptr uint64)  : int32 {.importc:"nng_msg_trim_u64", header:"<nng/ponng.h>".}



#// int nng_msg_dup(nng_msg **, const nng_msg *)

proc nng_msg_dup*(a0 : ptr ptr nng_msg, a1 : ptr nng_msg)  : int32 {.importc:"nng_msg_dup", header:"<nng/ponng.h>".}


#// void nng_msg_clear(nng_msg *)

proc nng_msg_clear*(a0 : ptr nng_msg)   {.importc:"nng_msg_clear", header:"<nng/ponng.h>".}


#// void nng_msg_header_clear(nng_msg *)

proc nng_msg_header_clear*(a0 : ptr nng_msg)   {.importc:"nng_msg_header_clear", header:"<nng/ponng.h>".}


type nng_pipe* {.final, importc:"nng_pipe", header:"<nng/ponng.h>".} = object

#// void nng_msg_set_pipe(nng_msg *, nng_pipe)

proc nng_msg_set_pipe*(a0 : ptr nng_msg, a1 : nng_pipe)   {.importc:"nng_msg_set_pipe", header:"<nng/ponng.h>".}



#// nng_pipe nng_msg_get_pipe(const nng_msg *)

proc nng_msg_get_pipe*(a0 : ptr nng_msg)  : nng_pipe {.importc:"nng_msg_get_pipe", header:"<nng/ponng.h>".}


#// int nng_msg_getopt(nng_msg *, int, void *, size_t *)

proc nng_msg_getopt*(a0 : ptr nng_msg, a1 : int32, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_msg_getopt", header:"<nng/ponng.h>".}


#// int nng_pipe_getopt(nng_pipe, const char *, void *, size_t *)

proc nng_pipe_getopt*(a0 : nng_pipe, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_pipe_getopt", header:"<nng/ponng.h>".}


#// int nng_pipe_getopt_bool(nng_pipe, const char *, _Bool *)

proc nng_pipe_getopt_bool*(a0 : nng_pipe, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_pipe_getopt_bool", header:"<nng/ponng.h>".}


#// int nng_pipe_getopt_int(nng_pipe, const char *, int *)

proc nng_pipe_getopt_int*(a0 : nng_pipe, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_pipe_getopt_int", header:"<nng/ponng.h>".}



#// int nng_pipe_getopt_ms(nng_pipe, const char *, nng_duration *)

proc nng_pipe_getopt_ms*(a0 : nng_pipe, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_pipe_getopt_ms", header:"<nng/ponng.h>".}


#// int nng_pipe_getopt_size(nng_pipe, const char *, size_t *)

proc nng_pipe_getopt_size*(a0 : nng_pipe, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_pipe_getopt_size", header:"<nng/ponng.h>".}



#// int nng_pipe_getopt_sockaddr(nng_pipe, const char *, nng_sockaddr *)

proc nng_pipe_getopt_sockaddr*(a0 : nng_pipe, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_pipe_getopt_sockaddr", header:"<nng/ponng.h>".}


#// int nng_pipe_getopt_uint64(nng_pipe, const char *, uint64_t *)

proc nng_pipe_getopt_uint64*(a0 : nng_pipe, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_pipe_getopt_uint64", header:"<nng/ponng.h>".}


#// int nng_pipe_getopt_ptr(nng_pipe, const char *, void **)

proc nng_pipe_getopt_ptr*(a0 : nng_pipe, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_pipe_getopt_ptr", header:"<nng/ponng.h>".}


#// int nng_pipe_getopt_string(nng_pipe, const char *, char **)

proc nng_pipe_getopt_string*(a0 : nng_pipe, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_pipe_getopt_string", header:"<nng/ponng.h>".}


#// int nng_pipe_get(nng_pipe, const char *, void *, size_t *)

proc nng_pipe_get*(a0 : nng_pipe, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_pipe_get", header:"<nng/ponng.h>".}


#// int nng_pipe_get_bool(nng_pipe, const char *, _Bool *)

proc nng_pipe_get_bool*(a0 : nng_pipe, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_pipe_get_bool", header:"<nng/ponng.h>".}


#// int nng_pipe_get_int(nng_pipe, const char *, int *)

proc nng_pipe_get_int*(a0 : nng_pipe, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_pipe_get_int", header:"<nng/ponng.h>".}



#// int nng_pipe_get_ms(nng_pipe, const char *, nng_duration *)

proc nng_pipe_get_ms*(a0 : nng_pipe, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_pipe_get_ms", header:"<nng/ponng.h>".}


#// int nng_pipe_get_size(nng_pipe, const char *, size_t *)

proc nng_pipe_get_size*(a0 : nng_pipe, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_pipe_get_size", header:"<nng/ponng.h>".}


#// int nng_pipe_get_uint64(nng_pipe, const char *, uint64_t *)

proc nng_pipe_get_uint64*(a0 : nng_pipe, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_pipe_get_uint64", header:"<nng/ponng.h>".}


#// int nng_pipe_get_string(nng_pipe, const char *, char **)

proc nng_pipe_get_string*(a0 : nng_pipe, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_pipe_get_string", header:"<nng/ponng.h>".}


#// int nng_pipe_get_ptr(nng_pipe, const char *, void **)

proc nng_pipe_get_ptr*(a0 : nng_pipe, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_pipe_get_ptr", header:"<nng/ponng.h>".}



#// int nng_pipe_get_addr(nng_pipe, const char *, nng_sockaddr *)

proc nng_pipe_get_addr*(a0 : nng_pipe, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_pipe_get_addr", header:"<nng/ponng.h>".}


#// int nng_pipe_close(nng_pipe)

proc nng_pipe_close*(a0 : nng_pipe)  : int32 {.importc:"nng_pipe_close", header:"<nng/ponng.h>".}


#// int nng_pipe_id(nng_pipe)

proc nng_pipe_id*(a0 : nng_pipe)  : int32 {.importc:"nng_pipe_id", header:"<nng/ponng.h>".}



#// nng_socket nng_pipe_socket(nng_pipe)

proc nng_pipe_socket*(a0 : nng_pipe)  : nng_socket {.importc:"nng_pipe_socket", header:"<nng/ponng.h>".}



#// nng_dialer nng_pipe_dialer(nng_pipe)

proc nng_pipe_dialer*(a0 : nng_pipe)  : nng_dialer {.importc:"nng_pipe_dialer", header:"<nng/ponng.h>".}



#// nng_listener nng_pipe_listener(nng_pipe)

proc nng_pipe_listener*(a0 : nng_pipe)  : nng_listener {.importc:"nng_pipe_listener", header:"<nng/ponng.h>".}

# enum nng_flag_enum
const NNG_FLAG_ALLOC* = 1
const NNG_FLAG_NONBLOCK* = 2
proc nng_flag_enum_tostr(v : int) : string =
  let iv = cast[int32](v)
  var ecname = "nng_flag_enum_unknown"
  if iv != iv: discard
  elif iv == NNG_FLAG_ALLOC: ecname = "NNG_FLAG_ALLOC"
  elif iv == NNG_FLAG_NONBLOCK: ecname = "NNG_FLAG_NONBLOCK"
  else: discard
  return ecname
type nng_stat* {.final, importc:"nng_stat", header:"<nng/ponng.h>".} = object

#// int nng_stats_get(nng_stat **)

proc nng_stats_get*(a0 : ptr ptr nng_stat)  : int32 {.importc:"nng_stats_get", header:"<nng/ponng.h>".}


#// void nng_stats_free(nng_stat *)

proc nng_stats_free*(a0 : ptr nng_stat)   {.importc:"nng_stats_free", header:"<nng/ponng.h>".}


#// void nng_stats_dump(nng_stat *)

proc nng_stats_dump*(a0 : ptr nng_stat)   {.importc:"nng_stats_dump", header:"<nng/ponng.h>".}



#// nng_stat * nng_stat_next(nng_stat *)

proc nng_stat_next*(a0 : ptr nng_stat)  : ptr nng_stat {.importc:"nng_stat_next", header:"<nng/ponng.h>".}



#// nng_stat * nng_stat_child(nng_stat *)

proc nng_stat_child*(a0 : ptr nng_stat)  : ptr nng_stat {.importc:"nng_stat_child", header:"<nng/ponng.h>".}


#// const char * nng_stat_name(nng_stat *)

proc nng_stat_name*(a0 : ptr nng_stat)  : cstring {.importc:"nng_stat_name", header:"<nng/ponng.h>".}


#// int nng_stat_type(nng_stat *)

proc nng_stat_type*(a0 : ptr nng_stat)  : int32 {.importc:"nng_stat_type", header:"<nng/ponng.h>".}

# enum nng_stat_type_enum
const NNG_STAT_SCOPE* = 0
const NNG_STAT_LEVEL* = 1
const NNG_STAT_COUNTER* = 2
const NNG_STAT_STRING* = 3
const NNG_STAT_BOOLEAN* = 4
const NNG_STAT_ID* = 5
proc nng_stat_type_enum_tostr(v : int) : string =
  let iv = cast[int32](v)
  var ecname = "nng_stat_type_enum_unknown"
  if iv != iv: discard
  elif iv == NNG_STAT_SCOPE: ecname = "NNG_STAT_SCOPE"
  elif iv == NNG_STAT_LEVEL: ecname = "NNG_STAT_LEVEL"
  elif iv == NNG_STAT_COUNTER: ecname = "NNG_STAT_COUNTER"
  elif iv == NNG_STAT_STRING: ecname = "NNG_STAT_STRING"
  elif iv == NNG_STAT_BOOLEAN: ecname = "NNG_STAT_BOOLEAN"
  elif iv == NNG_STAT_ID: ecname = "NNG_STAT_ID"
  else: discard
  return ecname

#// int nng_stat_unit(nng_stat *)

proc nng_stat_unit*(a0 : ptr nng_stat)  : int32 {.importc:"nng_stat_unit", header:"<nng/ponng.h>".}

# enum nng_unit_enum
const NNG_UNIT_NONE* = 0
const NNG_UNIT_BYTES* = 1
const NNG_UNIT_MESSAGES* = 2
const NNG_UNIT_MILLIS* = 3
const NNG_UNIT_EVENTS* = 4
proc nng_unit_enum_tostr(v : int) : string =
  let iv = cast[int32](v)
  var ecname = "nng_unit_enum_unknown"
  if iv != iv: discard
  elif iv == NNG_UNIT_NONE: ecname = "NNG_UNIT_NONE"
  elif iv == NNG_UNIT_BYTES: ecname = "NNG_UNIT_BYTES"
  elif iv == NNG_UNIT_MESSAGES: ecname = "NNG_UNIT_MESSAGES"
  elif iv == NNG_UNIT_MILLIS: ecname = "NNG_UNIT_MILLIS"
  elif iv == NNG_UNIT_EVENTS: ecname = "NNG_UNIT_EVENTS"
  else: discard
  return ecname

#// uint64_t nng_stat_value(nng_stat *)

proc nng_stat_value*(a0 : ptr nng_stat)  : uint64 {.importc:"nng_stat_value", header:"<nng/ponng.h>".}


#// const char * nng_stat_string(nng_stat *)

proc nng_stat_string*(a0 : ptr nng_stat)  : cstring {.importc:"nng_stat_string", header:"<nng/ponng.h>".}


#// const char * nng_stat_desc(nng_stat *)

proc nng_stat_desc*(a0 : ptr nng_stat)  : cstring {.importc:"nng_stat_desc", header:"<nng/ponng.h>".}


#// uint64_t nng_stat_timestamp(nng_stat *)

proc nng_stat_timestamp*(a0 : ptr nng_stat)  : uint64 {.importc:"nng_stat_timestamp", header:"<nng/ponng.h>".}



#// int nng_device(nng_socket, nng_socket)

proc nng_device*(a0 : nng_socket, a1 : nng_socket)  : int32 {.importc:"nng_device", header:"<nng/ponng.h>".}

# enum nng_errno_enum
const NNG_EINTR* = 1
const NNG_ENOMEM* = 2
const NNG_EINVAL* = 3
const NNG_EBUSY* = 4
const NNG_ETIMEDOUT* = 5
const NNG_ECONNREFUSED* = 6
const NNG_ECLOSED* = 7
const NNG_EAGAIN* = 8
const NNG_ENOTSUP* = 9
const NNG_EADDRINUSE* = 10
const NNG_ESTATE* = 11
const NNG_ENOENT* = 12
const NNG_EPROTO* = 13
const NNG_EUNREACHABLE* = 14
const NNG_EADDRINVAL* = 15
const NNG_EPERM* = 16
const NNG_EMSGSIZE* = 17
const NNG_ECONNABORTED* = 18
const NNG_ECONNRESET* = 19
const NNG_ECANCELED* = 20
const NNG_ENOFILES* = 21
const NNG_ENOSPC* = 22
const NNG_EEXIST* = 23
const NNG_EREADONLY* = 24
const NNG_EWRITEONLY* = 25
const NNG_ECRYPTO* = 26
const NNG_EPEERAUTH* = 27
const NNG_ENOARG* = 28
const NNG_EAMBIGUOUS* = 29
const NNG_EBADTYPE* = 30
const NNG_EINTERNAL* = 1000
const NNG_ESYSERR* = 268435456
const NNG_ETRANERR* = 536870912
proc nng_errno_enum_tostr(v : int) : string =
  let iv = cast[int32](v)
  var ecname = "nng_errno_enum_unknown"
  if iv != iv: discard
  elif iv == NNG_EINTR: ecname = "NNG_EINTR"
  elif iv == NNG_ENOMEM: ecname = "NNG_ENOMEM"
  elif iv == NNG_EINVAL: ecname = "NNG_EINVAL"
  elif iv == NNG_EBUSY: ecname = "NNG_EBUSY"
  elif iv == NNG_ETIMEDOUT: ecname = "NNG_ETIMEDOUT"
  elif iv == NNG_ECONNREFUSED: ecname = "NNG_ECONNREFUSED"
  elif iv == NNG_ECLOSED: ecname = "NNG_ECLOSED"
  elif iv == NNG_EAGAIN: ecname = "NNG_EAGAIN"
  elif iv == NNG_ENOTSUP: ecname = "NNG_ENOTSUP"
  elif iv == NNG_EADDRINUSE: ecname = "NNG_EADDRINUSE"
  elif iv == NNG_ESTATE: ecname = "NNG_ESTATE"
  elif iv == NNG_ENOENT: ecname = "NNG_ENOENT"
  elif iv == NNG_EPROTO: ecname = "NNG_EPROTO"
  elif iv == NNG_EUNREACHABLE: ecname = "NNG_EUNREACHABLE"
  elif iv == NNG_EADDRINVAL: ecname = "NNG_EADDRINVAL"
  elif iv == NNG_EPERM: ecname = "NNG_EPERM"
  elif iv == NNG_EMSGSIZE: ecname = "NNG_EMSGSIZE"
  elif iv == NNG_ECONNABORTED: ecname = "NNG_ECONNABORTED"
  elif iv == NNG_ECONNRESET: ecname = "NNG_ECONNRESET"
  elif iv == NNG_ECANCELED: ecname = "NNG_ECANCELED"
  elif iv == NNG_ENOFILES: ecname = "NNG_ENOFILES"
  elif iv == NNG_ENOSPC: ecname = "NNG_ENOSPC"
  elif iv == NNG_EEXIST: ecname = "NNG_EEXIST"
  elif iv == NNG_EREADONLY: ecname = "NNG_EREADONLY"
  elif iv == NNG_EWRITEONLY: ecname = "NNG_EWRITEONLY"
  elif iv == NNG_ECRYPTO: ecname = "NNG_ECRYPTO"
  elif iv == NNG_EPEERAUTH: ecname = "NNG_EPEERAUTH"
  elif iv == NNG_ENOARG: ecname = "NNG_ENOARG"
  elif iv == NNG_EAMBIGUOUS: ecname = "NNG_EAMBIGUOUS"
  elif iv == NNG_EBADTYPE: ecname = "NNG_EBADTYPE"
  elif iv == NNG_EINTERNAL: ecname = "NNG_EINTERNAL"
  elif iv == NNG_ESYSERR: ecname = "NNG_ESYSERR"
  elif iv == NNG_ETRANERR: ecname = "NNG_ETRANERR"
  else: discard
  return ecname
type nng_url* {.final, importc:"nng_url", header:"<nng/ponng.h>".} = object

#// int nng_url_parse(nng_url **, const char *)

proc nng_url_parse*(a0 : ptr ptr nng_url, a1 : cstring)  : int32 {.importc:"nng_url_parse", header:"<nng/ponng.h>".}


#// void nng_url_free(nng_url *)

proc nng_url_free*(a0 : ptr nng_url)   {.importc:"nng_url_free", header:"<nng/ponng.h>".}



#// int nng_url_clone(nng_url **, const nng_url *)

proc nng_url_clone*(a0 : ptr ptr nng_url, a1 : ptr nng_url)  : int32 {.importc:"nng_url_clone", header:"<nng/ponng.h>".}

#// const char * nng_version()

proc nng_version*()  : cstring {.importc:"nng_version", header:"<nng/ponng.h>".}

type nng_stream* {.final, importc:"nng_stream", header:"<nng/ponng.h>".} = object

#// void nng_stream_free(nng_stream *)

proc nng_stream_free*(a0 : ptr nng_stream)   {.importc:"nng_stream_free", header:"<nng/ponng.h>".}


#// void nng_stream_close(nng_stream *)

proc nng_stream_close*(a0 : ptr nng_stream)   {.importc:"nng_stream_close", header:"<nng/ponng.h>".}



#// void nng_stream_send(nng_stream *, nng_aio *)

proc nng_stream_send*(a0 : ptr nng_stream, a1 : ptr nng_aio)   {.importc:"nng_stream_send", header:"<nng/ponng.h>".}



#// void nng_stream_recv(nng_stream *, nng_aio *)

proc nng_stream_recv*(a0 : ptr nng_stream, a1 : ptr nng_aio)   {.importc:"nng_stream_recv", header:"<nng/ponng.h>".}


#// int nng_stream_get(nng_stream *, const char *, void *, size_t *)

proc nng_stream_get*(a0 : ptr nng_stream, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_stream_get", header:"<nng/ponng.h>".}


#// int nng_stream_get_bool(nng_stream *, const char *, _Bool *)

proc nng_stream_get_bool*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_stream_get_bool", header:"<nng/ponng.h>".}


#// int nng_stream_get_int(nng_stream *, const char *, int *)

proc nng_stream_get_int*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_stream_get_int", header:"<nng/ponng.h>".}



#// int nng_stream_get_ms(nng_stream *, const char *, nng_duration *)

proc nng_stream_get_ms*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_stream_get_ms", header:"<nng/ponng.h>".}


#// int nng_stream_get_size(nng_stream *, const char *, size_t *)

proc nng_stream_get_size*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_stream_get_size", header:"<nng/ponng.h>".}


#// int nng_stream_get_uint64(nng_stream *, const char *, uint64_t *)

proc nng_stream_get_uint64*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_stream_get_uint64", header:"<nng/ponng.h>".}


#// int nng_stream_get_string(nng_stream *, const char *, char **)

proc nng_stream_get_string*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_stream_get_string", header:"<nng/ponng.h>".}


#// int nng_stream_get_ptr(nng_stream *, const char *, void **)

proc nng_stream_get_ptr*(a0 : ptr nng_stream, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_stream_get_ptr", header:"<nng/ponng.h>".}



#// int nng_stream_get_addr(nng_stream *, const char *, nng_sockaddr *)

proc nng_stream_get_addr*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_stream_get_addr", header:"<nng/ponng.h>".}


#// int nng_stream_set(nng_stream *, const char *, const void *, size_t)

proc nng_stream_set*(a0 : ptr nng_stream, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_stream_set", header:"<nng/ponng.h>".}


#// int nng_stream_set_bool(nng_stream *, const char *, _Bool)

proc nng_stream_set_bool*(a0 : ptr nng_stream, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_stream_set_bool", header:"<nng/ponng.h>".}


#// int nng_stream_set_int(nng_stream *, const char *, int)

proc nng_stream_set_int*(a0 : ptr nng_stream, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_stream_set_int", header:"<nng/ponng.h>".}



#// int nng_stream_set_ms(nng_stream *, const char *, nng_duration)

proc nng_stream_set_ms*(a0 : ptr nng_stream, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_stream_set_ms", header:"<nng/ponng.h>".}


#// int nng_stream_set_size(nng_stream *, const char *, size_t)

proc nng_stream_set_size*(a0 : ptr nng_stream, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_stream_set_size", header:"<nng/ponng.h>".}


#// int nng_stream_set_uint64(nng_stream *, const char *, uint64_t)

proc nng_stream_set_uint64*(a0 : ptr nng_stream, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_stream_set_uint64", header:"<nng/ponng.h>".}


#// int nng_stream_set_string(nng_stream *, const char *, const char *)

proc nng_stream_set_string*(a0 : ptr nng_stream, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_stream_set_string", header:"<nng/ponng.h>".}


#// int nng_stream_set_ptr(nng_stream *, const char *, void *)

proc nng_stream_set_ptr*(a0 : ptr nng_stream, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_stream_set_ptr", header:"<nng/ponng.h>".}



#// int nng_stream_set_addr(nng_stream *, const char *, const nng_sockaddr *)

proc nng_stream_set_addr*(a0 : ptr nng_stream, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_stream_set_addr", header:"<nng/ponng.h>".}

type nng_stream_dialer* {.final, importc:"nng_stream_dialer", header:"<nng/ponng.h>".} = object

#// int nng_stream_dialer_alloc(nng_stream_dialer **, const char *)

proc nng_stream_dialer_alloc*(a0 : ptr ptr nng_stream_dialer, a1 : cstring)  : int32 {.importc:"nng_stream_dialer_alloc", header:"<nng/ponng.h>".}



#// int nng_stream_dialer_alloc_url(nng_stream_dialer **, const nng_url *)

proc nng_stream_dialer_alloc_url*(a0 : ptr ptr nng_stream_dialer, a1 : ptr nng_url)  : int32 {.importc:"nng_stream_dialer_alloc_url", header:"<nng/ponng.h>".}


#// void nng_stream_dialer_free(nng_stream_dialer *)

proc nng_stream_dialer_free*(a0 : ptr nng_stream_dialer)   {.importc:"nng_stream_dialer_free", header:"<nng/ponng.h>".}


#// void nng_stream_dialer_close(nng_stream_dialer *)

proc nng_stream_dialer_close*(a0 : ptr nng_stream_dialer)   {.importc:"nng_stream_dialer_close", header:"<nng/ponng.h>".}



#// void nng_stream_dialer_dial(nng_stream_dialer *, nng_aio *)

proc nng_stream_dialer_dial*(a0 : ptr nng_stream_dialer, a1 : ptr nng_aio)   {.importc:"nng_stream_dialer_dial", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_set(nng_stream_dialer *, const char *, const void *, size_t)

proc nng_stream_dialer_set*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_stream_dialer_set", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_get(nng_stream_dialer *, const char *, void *, size_t *)

proc nng_stream_dialer_get*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_stream_dialer_get", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_get_bool(nng_stream_dialer *, const char *, _Bool *)

proc nng_stream_dialer_get_bool*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_stream_dialer_get_bool", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_get_int(nng_stream_dialer *, const char *, int *)

proc nng_stream_dialer_get_int*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_stream_dialer_get_int", header:"<nng/ponng.h>".}



#// int nng_stream_dialer_get_ms(nng_stream_dialer *, const char *, nng_duration *)

proc nng_stream_dialer_get_ms*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_stream_dialer_get_ms", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_get_size(nng_stream_dialer *, const char *, size_t *)

proc nng_stream_dialer_get_size*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_stream_dialer_get_size", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_get_uint64(nng_stream_dialer *, const char *, uint64_t *)

proc nng_stream_dialer_get_uint64*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_stream_dialer_get_uint64", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_get_string(nng_stream_dialer *, const char *, char **)

proc nng_stream_dialer_get_string*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_stream_dialer_get_string", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_get_ptr(nng_stream_dialer *, const char *, void **)

proc nng_stream_dialer_get_ptr*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_stream_dialer_get_ptr", header:"<nng/ponng.h>".}



#// int nng_stream_dialer_get_addr(nng_stream_dialer *, const char *, nng_sockaddr *)

proc nng_stream_dialer_get_addr*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_stream_dialer_get_addr", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_set_bool(nng_stream_dialer *, const char *, _Bool)

proc nng_stream_dialer_set_bool*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_stream_dialer_set_bool", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_set_int(nng_stream_dialer *, const char *, int)

proc nng_stream_dialer_set_int*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_stream_dialer_set_int", header:"<nng/ponng.h>".}



#// int nng_stream_dialer_set_ms(nng_stream_dialer *, const char *, nng_duration)

proc nng_stream_dialer_set_ms*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_stream_dialer_set_ms", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_set_size(nng_stream_dialer *, const char *, size_t)

proc nng_stream_dialer_set_size*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_stream_dialer_set_size", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_set_uint64(nng_stream_dialer *, const char *, uint64_t)

proc nng_stream_dialer_set_uint64*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_stream_dialer_set_uint64", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_set_string(nng_stream_dialer *, const char *, const char *)

proc nng_stream_dialer_set_string*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_stream_dialer_set_string", header:"<nng/ponng.h>".}


#// int nng_stream_dialer_set_ptr(nng_stream_dialer *, const char *, void *)

proc nng_stream_dialer_set_ptr*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_stream_dialer_set_ptr", header:"<nng/ponng.h>".}



#// int nng_stream_dialer_set_addr(nng_stream_dialer *, const char *, const nng_sockaddr *)

proc nng_stream_dialer_set_addr*(a0 : ptr nng_stream_dialer, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_stream_dialer_set_addr", header:"<nng/ponng.h>".}

type nng_stream_listener* {.final, importc:"nng_stream_listener", header:"<nng/ponng.h>".} = object

#// int nng_stream_listener_alloc(nng_stream_listener **, const char *)

proc nng_stream_listener_alloc*(a0 : ptr ptr nng_stream_listener, a1 : cstring)  : int32 {.importc:"nng_stream_listener_alloc", header:"<nng/ponng.h>".}



#// int nng_stream_listener_alloc_url(nng_stream_listener **, const nng_url *)

proc nng_stream_listener_alloc_url*(a0 : ptr ptr nng_stream_listener, a1 : ptr nng_url)  : int32 {.importc:"nng_stream_listener_alloc_url", header:"<nng/ponng.h>".}


#// void nng_stream_listener_free(nng_stream_listener *)

proc nng_stream_listener_free*(a0 : ptr nng_stream_listener)   {.importc:"nng_stream_listener_free", header:"<nng/ponng.h>".}


#// void nng_stream_listener_close(nng_stream_listener *)

proc nng_stream_listener_close*(a0 : ptr nng_stream_listener)   {.importc:"nng_stream_listener_close", header:"<nng/ponng.h>".}


#// int nng_stream_listener_listen(nng_stream_listener *)

proc nng_stream_listener_listen*(a0 : ptr nng_stream_listener)  : int32 {.importc:"nng_stream_listener_listen", header:"<nng/ponng.h>".}



#// void nng_stream_listener_accept(nng_stream_listener *, nng_aio *)

proc nng_stream_listener_accept*(a0 : ptr nng_stream_listener, a1 : ptr nng_aio)   {.importc:"nng_stream_listener_accept", header:"<nng/ponng.h>".}


#// int nng_stream_listener_set(nng_stream_listener *, const char *, const void *, size_t)

proc nng_stream_listener_set*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : pointer, a3 : uint64)  : int32 {.importc:"nng_stream_listener_set", header:"<nng/ponng.h>".}


#// int nng_stream_listener_get(nng_stream_listener *, const char *, void *, size_t *)

proc nng_stream_listener_get*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : pointer, a3 : ptr uint64)  : int32 {.importc:"nng_stream_listener_get", header:"<nng/ponng.h>".}


#// int nng_stream_listener_get_bool(nng_stream_listener *, const char *, _Bool *)

proc nng_stream_listener_get_bool*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr bool)  : int32 {.importc:"nng_stream_listener_get_bool", header:"<nng/ponng.h>".}


#// int nng_stream_listener_get_int(nng_stream_listener *, const char *, int *)

proc nng_stream_listener_get_int*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr int32)  : int32 {.importc:"nng_stream_listener_get_int", header:"<nng/ponng.h>".}



#// int nng_stream_listener_get_ms(nng_stream_listener *, const char *, nng_duration *)

proc nng_stream_listener_get_ms*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr nng_duration)  : int32 {.importc:"nng_stream_listener_get_ms", header:"<nng/ponng.h>".}


#// int nng_stream_listener_get_size(nng_stream_listener *, const char *, size_t *)

proc nng_stream_listener_get_size*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_stream_listener_get_size", header:"<nng/ponng.h>".}


#// int nng_stream_listener_get_uint64(nng_stream_listener *, const char *, uint64_t *)

proc nng_stream_listener_get_uint64*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr uint64)  : int32 {.importc:"nng_stream_listener_get_uint64", header:"<nng/ponng.h>".}


#// int nng_stream_listener_get_string(nng_stream_listener *, const char *, char **)

proc nng_stream_listener_get_string*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr ptr int8)  : int32 {.importc:"nng_stream_listener_get_string", header:"<nng/ponng.h>".}


#// int nng_stream_listener_get_ptr(nng_stream_listener *, const char *, void **)

proc nng_stream_listener_get_ptr*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_stream_listener_get_ptr", header:"<nng/ponng.h>".}



#// int nng_stream_listener_get_addr(nng_stream_listener *, const char *, nng_sockaddr *)

proc nng_stream_listener_get_addr*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_stream_listener_get_addr", header:"<nng/ponng.h>".}


#// int nng_stream_listener_set_bool(nng_stream_listener *, const char *, _Bool)

proc nng_stream_listener_set_bool*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : bool)  : int32 {.importc:"nng_stream_listener_set_bool", header:"<nng/ponng.h>".}


#// int nng_stream_listener_set_int(nng_stream_listener *, const char *, int)

proc nng_stream_listener_set_int*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : int32)  : int32 {.importc:"nng_stream_listener_set_int", header:"<nng/ponng.h>".}



#// int nng_stream_listener_set_ms(nng_stream_listener *, const char *, nng_duration)

proc nng_stream_listener_set_ms*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : nng_duration)  : int32 {.importc:"nng_stream_listener_set_ms", header:"<nng/ponng.h>".}


#// int nng_stream_listener_set_size(nng_stream_listener *, const char *, size_t)

proc nng_stream_listener_set_size*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_stream_listener_set_size", header:"<nng/ponng.h>".}


#// int nng_stream_listener_set_uint64(nng_stream_listener *, const char *, uint64_t)

proc nng_stream_listener_set_uint64*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : uint64)  : int32 {.importc:"nng_stream_listener_set_uint64", header:"<nng/ponng.h>".}


#// int nng_stream_listener_set_string(nng_stream_listener *, const char *, const char *)

proc nng_stream_listener_set_string*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : cstring)  : int32 {.importc:"nng_stream_listener_set_string", header:"<nng/ponng.h>".}


#// int nng_stream_listener_set_ptr(nng_stream_listener *, const char *, void *)

proc nng_stream_listener_set_ptr*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : pointer)  : int32 {.importc:"nng_stream_listener_set_ptr", header:"<nng/ponng.h>".}



#// int nng_stream_listener_set_addr(nng_stream_listener *, const char *, const nng_sockaddr *)

proc nng_stream_listener_set_addr*(a0 : ptr nng_stream_listener, a1 : cstring, a2 : ptr nng_sockaddr)  : int32 {.importc:"nng_stream_listener_set_addr", header:"<nng/ponng.h>".}


#// int nng_req0_open(nng_socket *)

proc nng_req0_open*(a0 : ptr nng_socket)  : int32 {.importc:"nng_req0_open", header:"<nng/ponng.h>".}


#// int nng_req0_open_raw(nng_socket *)

proc nng_req0_open_raw*(a0 : ptr nng_socket)  : int32 {.importc:"nng_req0_open_raw", header:"<nng/ponng.h>".}


#// int nng_rep0_open(nng_socket *)

proc nng_rep0_open*(a0 : ptr nng_socket)  : int32 {.importc:"nng_rep0_open", header:"<nng/ponng.h>".}


#// int nng_rep0_open_raw(nng_socket *)

proc nng_rep0_open_raw*(a0 : ptr nng_socket)  : int32 {.importc:"nng_rep0_open_raw", header:"<nng/ponng.h>".}


#// int nng_pub0_open(nng_socket *)

proc nng_pub0_open*(a0 : ptr nng_socket)  : int32 {.importc:"nng_pub0_open", header:"<nng/ponng.h>".}


#// int nng_pub0_open_raw(nng_socket *)

proc nng_pub0_open_raw*(a0 : ptr nng_socket)  : int32 {.importc:"nng_pub0_open_raw", header:"<nng/ponng.h>".}


#// int nng_sub0_open(nng_socket *)

proc nng_sub0_open*(a0 : ptr nng_socket)  : int32 {.importc:"nng_sub0_open", header:"<nng/ponng.h>".}


#// int nng_sub0_open_raw(nng_socket *)

proc nng_sub0_open_raw*(a0 : ptr nng_socket)  : int32 {.importc:"nng_sub0_open_raw", header:"<nng/ponng.h>".}

#// int nng_listen_rep0(int *, char *)

proc nng_listen_rep0*(sock : ptr int32, url : ptr uint8)  : int32 {.importc:"nng_listen_rep0", header:"<nng/ponng.h>".}

#// int nng_rep0_do(int, int (*)(int, char *, int, char **, int *))

proc nng_rep0_do*(sock : int32, h : pointer)  : int32 {.importc:"nng_rep0_do", header:"<nng/ponng.h>".}

#// int nng_dial_req0(int *, char *)

proc nng_dial_req0*(sock : ptr int32, url : ptr uint8)  : int32 {.importc:"nng_dial_req0", header:"<nng/ponng.h>".}

#// int nng_req0_do(int, char *, int, char **, int *)

proc nng_req0_do*(sock : int32, sdbuf : ptr uint8, sdsz : int32, rvbuf : ptr ptr int8, rvsz : ptr int32)  : int32 {.importc:"nng_req0_do", header:"<nng/ponng.h>".}

#// int nng_listen_pub0(int *, char *)

proc nng_listen_pub0*(sock : ptr int32, url : ptr uint8)  : int32 {.importc:"nng_listen_pub0", header:"<nng/ponng.h>".}

#// int nng_dial_sub0(int *, char *)

proc nng_dial_sub0*(sock : ptr int32, url : ptr uint8)  : int32 {.importc:"nng_dial_sub0", header:"<nng/ponng.h>".}

#// int nng_sub0_do(int, int (*)(int, char *, int))

proc nng_sub0_do*(sock : int32, h : pointer)  : int32 {.importc:"nng_sub0_do", header:"<nng/ponng.h>".}

#// int nng_listen_bus0(int *, char *)

proc nng_listen_bus0*(sock : ptr int32, url : ptr uint8)  : int32 {.importc:"nng_listen_bus0", header:"<nng/ponng.h>".}

#// int nng_bus0_do(int, int (*)(int, char *, int))

proc nng_bus0_do*(sock : int32, h : pointer)  : int32 {.importc:"nng_bus0_do", header:"<nng/ponng.h>".}

#// int nng_listen_serveyor0(int *, char *)

proc nng_listen_serveyor0*(sock : ptr int32, url : ptr uint8)  : int32 {.importc:"nng_listen_serveyor0", header:"<nng/ponng.h>".}

#// int nng_serveror0_do(int, int (*)(int, char *, int))

proc nng_serveror0_do*(sock : int32, h : pointer)  : int32 {.importc:"nng_serveror0_do", header:"<nng/ponng.h>".}

#// int nng_dial_respondent0(int *, char *)

proc nng_dial_respondent0*(sock : ptr int32, url : ptr uint8)  : int32 {.importc:"nng_dial_respondent0", header:"<nng/ponng.h>".}

#// int nng_respondent0_do(int, int (*)(int, char *, int))

proc nng_respondent0_do*(sock : int32, h : pointer)  : int32 {.importc:"nng_respondent0_do", header:"<nng/ponng.h>".}

# main block end
